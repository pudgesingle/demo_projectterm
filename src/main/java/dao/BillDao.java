/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import dao.DaoInterface;
import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Customer;
import model.Product;
import model.Bill;
import model.BillAndProduct;
import model.User;

/**
 *
 * @author Zerocool
 */
public class BillDao implements DaoInterface<Bill> {

    @Override
    public int add(Bill object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO bill ( customer_id, user_id, total, discount) VALUES (?,?,?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, object.getCustomer().getId());
            stmt.setInt(2, object.getSeller().getId());
            stmt.setDouble(3, object.getTotal());
            stmt.setDouble(4, object.getDiscount());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();

            if (result.next()) {
                id = result.getInt(1);
                object.setId(id);
            }
            for (BillAndProduct r : object.getBillAndProduct()) {
                String sqlDetail = "INSERT INTO bill_and_product ( bill_id, product_id, price, amount ) VALUES ( ?,?,?,? )";
                PreparedStatement stmtDetail = conn.prepareStatement(sqlDetail);
                stmtDetail.setInt(1, r.getBill().getId());
                stmtDetail.setInt(2, r.getProduct().getId());
                stmtDetail.setDouble(3, r.getPrice());
                stmtDetail.setInt(4, r.getAmount());
                int rowDetail = stmtDetail.executeUpdate();
                ResultSet resultDetail = stmtDetail.getGeneratedKeys();
                if (resultDetail.next()) {
                    id = resultDetail.getInt(1);
                    r.setId(id);
                }
            }

        } catch (SQLException ex) {
            System.out.println("Error: to create bill");
        }

        db.close();
        return id;
    }

    @Override
    public ArrayList<Bill> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT b.id as id, created, customer_id,"
                    + "c.name as customer_name, c.surname as customer_surname, "
                    + "c.tel as customer_tel, c.point as customer_point, user_id,"
                    + " u.username as user_name,u.tel as user_tel, b.total as total,"
                    + " b.discount as discount FROM bill b,"
                    + "customer c,user u WHERE b.customer_id = c.id AND b.user_id"
                    + " = u.id ORDER BY created DESC";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("id");
                Date created = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(result.getString("created"));
                int customerId = result.getInt("customer_id");
                String customerName = result.getString("customer_name");
                String customerSurname = result.getString("customer_surname");
                String customerTel = result.getString("customer_tel");
                int customerPoint = result.getInt("customer_point");
                int userId = result.getInt("user_id");
                String userName = result.getString("user_name");
                String userTel = result.getString("user_tel");
                double total = result.getDouble("total");
                double discount = result.getDouble("discount");
                Bill bill = new Bill(id, created,
                        new User(userId, userName, userTel),
                        new Customer(customerId, customerName, customerSurname, customerTel, customerPoint),
                        discount);
                getBillAndProduct(conn, id, bill);
                list.add(bill);
            }
        } catch (SQLException ex) {
            System.out.println("Error: Unable to select all bill");
        } catch (ParseException ex) {
            System.out.println("Error: Date parsing all bill");
        }

        db.close();
        return list;
    }
    
        public ArrayList<Bill> getAllMonth() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT b.id as id, created, customer_id,"
                    + "c.name as customer_name, c.surname as customer_surname, "
                    + "c.tel as customer_tel, c.point as customer_point, user_id,"
                    + " u.username as user_name,u.tel as user_tel, b.total as total, b.discount as discount FROM bill b,"
                    + "customer c,user u WHERE strftime('%m', created) ="
                    + " strftime('%m', CURRENT_TIMESTAMP) AND b.customer_id = c.id AND b.user_id"
                    + " = u.id ORDER BY created DESC";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("id");
                Date created = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(result.getString("created"));
                int customerId = result.getInt("customer_id");
                String customerName = result.getString("customer_name");
                String customerSurname = result.getString("customer_surname");
                String customerTel = result.getString("customer_tel");
                int customerPoint = result.getInt("customer_point");
                int userId = result.getInt("user_id");
                String userName = result.getString("user_name");
                String userTel = result.getString("user_tel");
                double total = result.getDouble("total");
                double discount = result.getDouble("discount");
                Bill bill = new Bill(id, created,
                        new User(userId, userName, userTel),
                        new Customer(customerId, customerName, customerSurname, customerTel, customerPoint),
                        discount);
                getBillAndProduct(conn, id, bill);
                list.add(bill);
            }
        } catch (SQLException ex) {
            System.out.println("Error: Unable to select all bill");
        } catch (ParseException ex) {
            System.out.println("Error: Date parsing all bill");
        }

        db.close();
        return list;
    }

    @Override
    public Bill get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT b.id as id, created, customer_id, c.name as customer_name,"
                    + "c.surname as customer_surname, c.tel as customer_tel, c.point as point,"
                    + "user_id, u.username as user_name,u.tel as user_tel,"
                    + " total, discount FROM bill b,customer c,user u "
                    + "WHERE b.id = ? AND b.customer_id = c.id AND b.user_id = u.id";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                int rid = result.getInt("id");
                Date created = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(result.getString("created"));
                int customerId = result.getInt("customer_id");
                String customerName = result.getString("customer_name");
                String customerSurname = result.getString("customer_surname");
                String customerTel = result.getString("customer_tel");
                int customerPoint = result.getInt("point");
                int userId = result.getInt("user_id");
                String userName = result.getString("user_name");
                String userTel = result.getString("user_tel");
                double total = result.getDouble("total");
                double discount = result.getDouble("discount");
                Bill bill = new Bill(rid, created,
                        new User(userId, userName, userTel),
                        new Customer(customerId, customerName, customerSurname, customerTel, customerPoint),
                        discount);

                getBillAndProduct(conn, id, bill);

                return bill;
            }
        } catch (SQLException ex) {
            System.out.println("Error: Unable to select bill id " + id + " !!" + ex.getMessage());
        } catch (ParseException ex) {
            System.out.println("Error: Date parsing all bill" + ex.getMessage());
        }

        return null;
    }

    private void getBillAndProduct(Connection conn, int id, Bill bill) throws SQLException {
        String sqlDetail = "SELECT bp.id as id, "
                + "bill_id, "
                + "product_id, "
                + "p.name as product_name, "
                + "p.price as product_price, "
                + "bp.price as price, amount "
                + "FROM bill_and_product bp,product p "
                + "WHERE bill_id = ? AND bp.product_id = p.id;";
        PreparedStatement stmtDetail = conn.prepareStatement(sqlDetail);
        stmtDetail.setInt(1, id);
        ResultSet resultDetail = stmtDetail.executeQuery();

        while (resultDetail.next()) {
            int billId = resultDetail.getInt("id");
            int productId = resultDetail.getInt("product_id");
            String productName = resultDetail.getString("product_name");
            double productPrice = resultDetail.getDouble("product_price");
            double price = resultDetail.getDouble("price");
            int amount = resultDetail.getInt("amount");
            Product product = new Product(productId, productName, productPrice);
            bill.addRecieptDetail(billId, product, amount, price);
        }
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM bill WHERE id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();

        } catch (SQLException ex) {
            System.out.println("Error: Unable to delete bill id " + id + " !!");
        }

        db.close();
        return row;
    }

    @Override
    public int update(Bill object) {
//        Connection conn = null;
//        Database db = Database.getInstance();
//        conn = db.getConnection();
//        int row = 0;
//        try {
//            String sql = "UPDATE receipt SET name = ?, price =? WHERE id = ?";
//            PreparedStatement stmt = conn.prepareStatement(sql);
//            stmt.setString(1, object.getName());
//            stmt.setDouble(2, object.getPrice());
//            stmt.setInt(3, object.getId());
//            row = stmt.executeUpdate();
//
//        } catch (SQLException ex) {
//            Logger.getLogger(TestReceipt.class.getName()).log(Level.SEVERE, null, ex);
//        }
//
//        db.close();
        return 0;
    }

    public static void main(String[] args) {
        Product p1 = new Product(1, "ลาเต้", 50);
        Product p2 = new Product(2, "เอสเพรสโซ่", 40);
        User seller = new User(1, "emplo", "0812345679");
        Customer customer = new Customer(3, "asdasd", "asfsaf", "0848007884", 0);
        Bill bill = new Bill(seller, customer,0);
        bill.addRecieptDetail(p1, 1);
        bill.addRecieptDetail(p2, 3);
        System.out.println(bill);
        BillDao dao = new BillDao();
        System.out.println("id =" + dao.add(bill));
        System.out.println("Recept after add: " + bill);
        System.out.println("Get all" + dao.getAll());

        Bill newbill = dao.get(bill.getId());
        System.out.println("New bill :" + newbill);
        dao.delete(bill.getId());
    }

}
