/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import com.mycompany.projecttarm.Test;
import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Product;
import model.ProductDetail;
import model.Stock;

/**
 *
 * @author PlugPC
 */
public class ProductDetailDao implements DaoInterface<ProductDetail> {

    @Override
    public int add(ProductDetail object) {
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();
        int id = -1;

        try {
            String sql = "INSERT INTO product_detail (product_id,stock_id,detail_qty,detail_unit)\n"
                    + "VALUES (?,?,?,?)";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, object.getProduct().getId());
            stmt.setInt(2, object.getStock().getId());
            stmt.setInt(3, object.getQty());
            stmt.setString(4, object.getUnit());

            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
            }

        } catch (SQLException ex) {
            Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<ProductDetail> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT pd.id as id,\n"
                    + "       product_id,\n"
                    + "       p.name as product_name,\n"
                    + "       stock_id,\n"
                    + "       s.name as stock_name,\n"
                    + "       detail_qty,\n"
                    + "       detail_unit\n"
                    + "  FROM product_detail pd,product p,stock s\n"
                    + "  WHERE pd.product_id = p.id AND pd.stock_id = s.id;";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("id");
                int product_id = result.getInt("product_id");
                String product_name = result.getString("product_name");
                int stock_id = result.getInt("stock_id");
                String stock_name = result.getString("stock_name");
                int detail_qty = result.getInt("detail_qty");
                String detail_unit = result.getString("detail_unit");
                ProductDetail productDetail = new ProductDetail(id, new Product(product_id, product_name, 0), new Stock(stock_id, stock_name, 0, "", 0, 0, ""), detail_qty, detail_unit);
                list.add(productDetail);
            }
        } catch (SQLException ex) {
            System.out.println("Error: don't get all Storage" + ex.getMessage());
        }
        db.close();
        return list;
    }

    @Override
    public ProductDetail get(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM product_detail WHERE id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();

        } catch (SQLException ex) {
            System.out.println("Error: Unable to delete product_detail id " + id + " !!");
        }

        db.close();
        return row;
    }

    @Override
    public int update(ProductDetail object) {
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE product_detail SET detail_qty = ?, detail_unit = ? WHERE id = ?";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, object.getQty());
            stmt.setString(2, object.getUnit());
            stmt.setInt(3, object.getId());
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return row;
    }

    public static void main(String[] args) {
        Product p1 = new Product(1, "ลาเต้", 55.0);
        Stock s2 = new Stock(2, "เคนย่า", 5, "ถุง", 540.0, 200, "กรัม");
//        ProductDetail productDetail = new ProductDetail(-1, p1, s2, 5, "กรัม");
        ProductDetailDao dao = new ProductDetailDao();
//        int id = dao.add(productDetail);
//        System.out.println(id);
        System.out.print(dao.getAll());
        int id = dao.update(new ProductDetail(3, p1, s2, 2, "กรัม"));
        System.out.println(id);
        System.out.print(dao.getAll());
    }

}
