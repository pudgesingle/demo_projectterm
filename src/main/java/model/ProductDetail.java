/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author PlugPC
 */
public class ProductDetail {
    private int id;
    private Product product;
    private Stock stock;
    private int qty;
    private String unit;

    public ProductDetail(int id, Product product, Stock stock, int qty, String unit) {
        this.id = id;
        this.product = product;
        this.stock = stock;
        this.qty = qty;
        this.unit = unit;
    }

    public ProductDetail(Product product, Stock stock, int qty, String unit) {
        this(-1,product,stock,qty,unit);
    }

    public int getId() {
        return id;
    }

    public Product getProduct() {
        return product;
    }

    public Stock getStock() {
        return stock;
    }

    public int getQty() {
        return qty;
    }

    public String getUnit() {
        return unit;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    @Override
    public String toString() {
        return "ProductDetail{" + "id=" + id + ", product=" + product + ", stock=" + stock + ", qty=" + qty + ", unit=" + unit + '}';
    }
    
    
}
